<?php 
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
include("connect.php");
$loginid = $_SESSION["uid"];

$sdate = $mysql->query("SELECT DISTINCT book_time.Date
FROM book_time
INNER JOIN rides
ON book_time.Rides_ID = rides.Rides_ID
WHERE book_time.Date >= NOW() AND book_time.Login_ID = '{$loginid}'
ORDER BY book_time.Date DESC");
$num = mysqli_num_rows($sdate);
if($num == 0){
    echo "<script>
    setTimeout(function() {
        swal({
            title: \"Not found your recent schedule\",
            text: \"We coudn't find your recent schedule. Would you like to buy a ticket?\",
            type: \"warning\",
            showCancelButton: true,
            confirmButtonColor: \"#DD6B55\",
            confirmButtonText: \"Buy a ticket\",
            cancelButtonText: \"Back\",
            closeOnClickOutside: false,
        }, function(isConfirm) {
            if(isConfirm){
            window.location = \"ticket.php\";}
            else{
                window.location = \"index.php\";
            }
        });
        
    }, 1000);
        </script>"; 

}

?>