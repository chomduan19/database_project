<?php
include("connect.php");

session_start(); 
$login_id = $_SESSION["uid"];


?>

<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.6">
    <title>Buy Ticket-DatabaseLand</title>

    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Sofia&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">


    <style>
        a {
            text-decoration: none;
        }

        #more {
            display: none;
        }

        #moreC {
            display: none;
        }

        #moreD {
            display: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">

</head>

<body>

    <header>
    <?php if(isset($_SESSION["uid"])) { ?>
        <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
            <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <a href="index.php">
                    <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">
                        DatabaseLand</li>
                </a>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z"
                                    clip-rule="evenodd" />
                            </svg> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em"
                                viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z"
                                    clip-rule="evenodd" />
                                <path fill-rule="evenodd"
                                    d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z"
                                    clip-rule="evenodd" />
                            </svg> Booking Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link btn btn-warning" href="signout.php"><svg class="bi bi-person" width="1em"
                                height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z"
                                    clip-rule="evenodd" />
                            </svg> Sign Out</a>
                    </li>
                </ul>
                
            </div>
        </nav>


    </header>


    <main role="main">
        <div class="container marketing" style="margin-top: 5%;">
            <h1 style="font-family: 'Sofia', cursive;">Buy Ticket<img src="img/S__86458513.jpg" width="60"></h1><br>
            <div class="row mb-2">
                <div class="col-md-6">
                    <div
                        class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block mb-2 text-primary">Ticket: A</strong>
                            <h3 class="mb-0">Easy Pass</h3>
                            <div class="mb-1 text-muted">for pass the door only.</div>
                            <p class="card-text mb-auto">This ticket included: <br>
                                - Enter the door<br>
                                - Wonderful Parade<br>
                                - Cuttie Dogie Show <br>
                                - Angry Bird Show</p><br>
                            <h6>Price: 250 Baht</h6>

                            <!--เพิ่มตรงรี้-->

                            <form action="payment.php" method="post">
                                        <input type="hidden" name="TicketID" value="00001">
                                     
                                      <!--  <input type="hidden" name="LoginID" value="vatinejill"> ถ้าจะส่งแอคเค้าไปด้วย -->
                                        <input type="submit" class="btn btn-success" name="submit" id="buyA" value="Buy Now">
                            </form>
                            <!--เพิ่มตรงรี้-->


                        </div>
                        <div class="col-auto d-none d-lg-block"><br><br><br><br>
                            <img src="img/IMG_0449.JPG" alt="" width="300">
                        </div>
                    </div>
                </div>
                <!-- </div>
        <div class="row mb-2"> -->
                <div class="col-md-6">
                    <div
                        class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block mb-2 text-success">Ticket: B</strong>
                            <h3 class="mb-0">Limited Pass</h3>
                            <div class="mb-1 text-muted">enjoy with 8 awesome things per 1 round</div>
                            <p class="card-text mb-auto">This ticket included: <br>
                                - Enter the door<br><span id="dots">...</span><span id="more">
                                    - Tornado<br>
                                    - Hurricane<br>
                                    - Sky Coaster <br>
                                    - Tea Cup <br>
                                    - Mad Tea Cup<br>
                                    - Wonderful Parade<br>
                                    - Cuttie Dogie Show <br>
                                    - Angry Bird Show</p></span>
                            <a onclick="myFunction()" id="myBtn" style="text-decoration:slateblue underline">Read
                                more</a>
                            <br>
                            <h6>Price: 500 Baht</h6>
                            <!--เพิ่มตรงรี้-->

                            <form action="payment.php" method="post">
                            <input type="hidden" name="TicketID" value="00002">
                           <!--  <input type="hidden" name="LoginID" value="vatinejill"> ถ้าจะส่งแอคเค้าไปด้วย -->
                                        <input type="submit" class="btn btn-success" id="buyB" value="Buy Now">
                            </form>
                            <!--เพิ่มตรงรี้-->
                        </div>
                        <div class="col-auto d-none d-lg-block"><br><br><br>
                            <img src="img/Hurricane.jpg" alt="" width="300">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-md-6">
                    <div
                        class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block mb-2 text-warning">Ticket: C</strong>
                            <h3 class="mb-0">Visa Pass</h3>
                            <div class="mb-1 text-muted">enjoy with 8 awesome things unlimit round</div>
                            <p class="card-text mb-auto">This ticket included: <br>
                                - Enter the door<br>
                                - Tornado<br><span id="dotsC">...</span><span id="moreC">
                                    - Hurricane<br>
                                    - Sky Coaster <br>
                                    - Tea Cup <br>
                                    - Mad Tea Cup<br>
                                    - Wonderful Parade<br>
                                    - Cuttie Dogie Show <br>
                                    - Angry Bird Show</p></span>
                            <a onclick="myFunctionC()" id="myBtnC" style="text-decoration:slateblue underline">Read
                                more</a>
                            <br>
                            <h6>Price: 600 Baht</h6>
                            <!--เพิ่มตรงรี้-->

                            <form action="payment.php" method="post">
                                        <input type="hidden" name="TicketID" value="00003">
                                       <!--  <input type="hidden" name="LoginID" value="vatinejill"> ถ้าจะส่งแอคเค้าไปด้วย -->
                                        <input type="submit" class="btn btn-success" name="submit" id="buyA" value="Buy Now">
                            </form>
                            <!--เพิ่มตรงรี้-->
                        </div>
                        <div class="col-auto d-none d-lg-block"><br><br><br><br>
                            <img src="img/sky.jpg" alt="" width="300">
                        </div>
                    </div>
                </div>
                <!-- </div>
          <div class="row mb-2"> -->
                <div class="col-md-6">
                    <div
                        class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <strong class="d-inline-block mb-2 text-danger">Ticket: D</strong>
                            <h3 class="mb-0">UnLimited Pass</h3>
                            <div class="mb-1 text-muted">enjoy with all awesome things unlimit round</div>
                            <p class="card-text mb-auto">This ticket included: <br>
                                - Enter the door<br><span id="dotsD">...</span><span id="moreD">
                                    - Tornado<br>
                                    - Hurricane<br>
                                    - Vikings<br>
                                    - Grand Canyon<br>
                                    - Sky Coaster <br>
                                    - Super Splash<br>
                                    - Tea Cup <br>
                                    - Mad Tea Cup<br>
                                    - Wonderful Parade<br>
                                    - Cuttie Dogie Show <br>
                                    - Angry Bird Show</p></span>
                            <a onclick="myFunctionD()" id="myBtnD" style="text-decoration:slateblue underline">Read
                                more</a>
                            <br>
                            <h6>Price: 1000 Baht</h6>
                            <!--เพิ่มตรงรี้-->

                            <form action="payment.php" method="post">
                                        <input type="hidden" name="TicketID" value="00004">
                                       <!--  <input type="hidden" name="LoginID" value="vatinejill"> ถ้าจะส่งแอคเค้าไปด้วย -->
                                        <input type="submit" class="btn btn-success" name="submit" id="buyA" value="Buy Now">
                            </form>
                            <!--เพิ่มตรงรี้-->
                        </div>
                        <div class="col-auto d-none d-lg-block"><br><br><br><br>
                            <img src="img/grand.png" alt="" width="300">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- FOOTER -->
        <footer class="container">
            <p class="float-right"><a href="#">Back to top</a></p>
            <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>
    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
        crossorigin="anonymous"></script>

    <script>
        function myFunction() {
            var dots = document.getElementById("dots");
            var moreText = document.getElementById("more");
            var btnText = document.getElementById("myBtn");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Read more";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "Read less";
                moreText.style.display = "inline";
            }
        }
    </script>
    <script>
        function myFunctionC() {
            var dots = document.getElementById("dotsC");
            var moreText = document.getElementById("moreC");
            var btnText = document.getElementById("myBtnC");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Read more";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "Read less";
                moreText.style.display = "inline";
            }
        }
    </script>
    <script>
        function myFunctionD() {
            var dots = document.getElementById("dotsD");
            var moreText = document.getElementById("moreD");
            var btnText = document.getElementById("myBtnD");

            if (dots.style.display === "none") {
                dots.style.display = "inline";
                btnText.innerHTML = "Read more";
                moreText.style.display = "none";
            } else {
                dots.style.display = "none";
                btnText.innerHTML = "Read less";
                moreText.style.display = "inline";
            }
        }
    </script>
<!-- else login yet         -->
<?php } else { ?>
    <script>
        alert("Please sign in before.");
        location.href = "signin.php"
    </script>
<?php } ?>
</body>



</html>