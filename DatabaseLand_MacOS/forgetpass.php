<!DOCTYPE html>

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.js"></script>
<?php
    session_start();

   include('connect.php');
    if($_SERVER['REQUEST_METHOD']==='POST'){

        $login = $_POST['loginid'];
        $mail = $_POST['email'];
    
        $s = "SELECT * FROM member WHERE Login_ID = '$login'";
        $e = "SELECT * FROM member WHERE E_mail = '$mail'";
    
        $result_s = mysqli_query($mysql,$s);
        $result_e = mysqli_query($mysql,$e);
       
        $num1 = mysqli_num_rows($result_s);
        $num2 = mysqli_num_rows($result_e);
    
        $r = "SELECT * FROM member WHERE Login_ID = '$login' AND E_mail = '$mail' LIMIT 1";
        $result_r = mysqli_query($mysql,$r);
        $num3 = mysqli_num_rows($result_r);
 
        $msg = "";
  
        if($num1 == 1 && $num2 == 1 && $num3 == 1){
            $_SESSION["loginld"] = $login;
            $_SESSION["email"] = $mail;
            // $emailto='{$mail}'; //อีเมล์ผู้รับ<br>
            // $subject= "Reset password Databaseland"; //หัวข้อ<br>
            // $header.= "Content-type: text/html; charset=utf-8\n";
            // $header.="from: DatabaseLand E-mail : Databaseland@gmail.com"; //ชื่อและอีเมลผู้ส่ง<br>
            // $messages.= "To '{$login}' <br> We had received your request, so this is your verification code \"123456\""; //ข้อความ<br>
            
            // $send_mail = mail($emailto,$subject,$messages,$header);
            // if(!$send_mail)
            // {
            //     echo"ยังไม่สามารถส่งเมลล์ได้ในขณะนี้";
            // }
            header("Location: resetpass.php");
        }
        else if($num1 == 1 && $num3 != 1 ){
            $msg = "Login ID and Email don't match.";
            echo "<script>
            setTimeout(function() {
            swal({
                title: \"$msg\",
                text: \"Please try again\",
                type: \"error\",
                showCancelButton: false,
                confirmButtonColor: \"#f44336\",
                confirmButtonText: \"OK\",
                
                closeOnClickOutside: false,
            }, function(isConfirm) {
                if(isConfirm){
                    window.location = \"forgetpass.php\";               
                    
                }
                
            });
            
        }, 500);
            </script>"; 
            
        }else if($num1 != 1){
            $msg = "This login ID never sign up.";
            echo "<script>
            setTimeout(function() {
            swal({
                title: \"$msg\",
                text: \"Please try again\",
                type: \"error\",
                showCancelButton: false,
                confirmButtonColor: \"#f44336\",
                confirmButtonText: \"OK\",
                
                closeOnClickOutside: false,
            }, function(isConfirm) {
                if(isConfirm){
                    window.location = \"forgetpass.php\";               
                    
                }
                
            });
            
        }, 500);
            </script>"; 

        }
        else if($num2 != 1){
            $msg = "This email never sign up.";
            echo "<script>
            setTimeout(function() {
            swal({
                title: \"$msg\",
                text: \"Please try again\",
                type: \"error\",
                showCancelButton: false,
                confirmButtonColor: \"#f44336\",
                confirmButtonText: \"OK\",
                
                closeOnClickOutside: false,
            }, function(isConfirm) {
                if(isConfirm){
                    window.location = \"forgetpass.php\";               
                    
                }
                
            });
            
        }, 500);
            </script>"; 

        }
        else if($num1 != 1 && $num2 != 1){
            $msg = "This account never sign up.";
            echo "<script>
            setTimeout(function() {
            swal({
                title: \"$msg\",
                text: \"Please try again\",
                type: \"error\",
                showCancelButton: false,
                confirmButtonColor: \"#f44336\",
                confirmButtonText: \"OK\",
                
                closeOnClickOutside: false,
            }, function(isConfirm) {
                if(isConfirm){
                    window.location = \"forgetpass.php\";               
                    
                }
                
            });
            
        }, 500);
            </script>"; 

        }
    }

?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Forget password-DatabaseLand</title>
    <!-- Bootstrap core CSS -->
    <link href="/docs/4.4/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Clicker+Script&family=Sofia&display=swap" rel="stylesheet">
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>


    <style>
        a {
            text-decoration: none;
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">
</head>

<body>
    <header>
        
        <nav class="navbar navbar-expand-md navbar-light fixed-top" style="background-color: #f7e37f;">
            <a class="navbar-brand" href="#"><img src="img/IMG_0427.PNG" width="30" height="30"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
               
                    <a href="index.php">
                        <li class="navbar-brand" style="font-family: 'Clicker Script', cursive; font-weight: bolder;">DatabaseLand</li>
                    </a>
                    <ul class="navbar-nav ml-auto"> 
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php"><svg class="bi bi-house" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 00.5.5h9a.5.5 0 00.5-.5V7h1v6.5a1.5 1.5 0 01-1.5 1.5h-9A1.5 1.5 0 012 13.5zm11-11V6l-2-2V2.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5z" clip-rule="evenodd"/>
                            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 011.414 0l6.647 6.646a.5.5 0 01-.708.708L8 2.207 1.354 8.854a.5.5 0 11-.708-.708L7.293 1.5z" clip-rule="evenodd"/>
                          </svg> Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ticket.php"><i class="fa fa-shopping-basket"></i> Buy Ticket</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Booktime.php"><svg class="bi bi-calendar" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M14 0H2a2 2 0 00-2 2v12a2 2 0 002 2h12a2 2 0 002-2V2a2 2 0 00-2-2zM1 3.857C1 3.384 1.448 3 2 3h12c.552 0 1 .384 1 .857v10.286c0 .473-.448.857-1 .857H2c-.552 0-1-.384-1-.857V3.857z" clip-rule="evenodd"/>
                            <path fill-rule="evenodd" d="M6.5 7a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm-9 3a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2zm3 0a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd"/>
                          </svg> Booking Time</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="Qr.php"><i class="fa fa-qrcode"></i> QR code</a>
                    </li>
               
                    <li class="nav-item">
                        <a class="nav-link btn btn-warning" href="signin.php"><svg class="bi bi-person" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM8 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0z" clip-rule="evenodd"/>
                          </svg> Sign in</a>
                    </li>
                </ul>
               
            </div>
        </nav>
      
        
    </header>
    

    <main role="main">
        <div class="container marketing">

            <div class="row" style="margin-top: 5%;">

                <div class="col-md-4"><br><br><br><br><br>
                   
                    <div class="col-sm-6"><img src="img/panda2.png" width="100%" style="margin-left:50%;"> </div>
                   
                </div>
                <div class="col-md-8"><br><br>

                    <h1 style="font-family: 'Sofia', cursive;">Forget Password <img src="img/cupcake.png" width="50"></h1><br>
                    <!-- เดี๋ยวแก้ให้match login and email -->
                    <form action="forgetpass.php" method="post"> 
                            <div class="form-group row">
                                    <label for="inputLname" class="col-sm-2 col-form-label">Login ID</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="loginid" name="loginid"
                                        required pattern="[A-Za-z0-9]*" minlength="6" maxlength="15">
        
                                    </div>
                                    <div class="col-sm-2"></div>
                                </div>
                            <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="email" class="form-control" id="inputEmail3" name="email"
                                         maxlength="100">
                                        <small class="text-muted">
                                                We'll sent a verification code to your email.
                                            </small>
                                    </div>
                                    <div class="col-sm-2"></div>
                                </div>

                        <div class="form-group row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                               
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>



        <!-- FOOTER -->
        <footer class="container">
            <p class="float-right"><a href="#">Back to top</a></p>
            <p>&copy; 2017-2019 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>
    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="/docs/4.4/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
        crossorigin="anonymous"></script>

</body>

</html>